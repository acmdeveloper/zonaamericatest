package com.zonamerica.empresaholandesa;

import java.util.Scanner;

public class EspecificacionesMuebles {
	
	int totalDimensionSuperficie=0;
	int totalDimensionVolumen=0;
	private Scanner sc ;
	
	public EspecificacionesMuebles() {
		sc = new Scanner(System.in);
	}
	
	
	public void generaMedidasMuebles() throws Exception{
		int dimensionSuperfieciePatas= 0;
		int dimensionVolumenPatas=0;
		int dimensionesSuperficieTabla=0;
		int dimensionesVolumenTabla=0;
		int cantPatas = 0;
		String opAccion ="";
		int dimensionSuperfieciePatasUnidad =0;
		do{
			
			escribeMenu();
			opAccion = sc.nextLine();
			sc = new Scanner(System.in);
			switch(opAccion){
				case "1":
					
					System.out.println("*****Dimensiones patas(Centimetros)**** ");
					cantPatas = getValor("Cantidad patas");
					dimensionSuperfieciePatasUnidad = getValor("Largo") *  getValor("Ancho"); 
					dimensionSuperfieciePatas = dimensionSuperfieciePatasUnidad * cantPatas;
					dimensionVolumenPatas = ((dimensionSuperfieciePatasUnidad *  getValor("Fondo") ) *  cantPatas  );
					System.out.println("*****Dimensiones tabla(Centimetros)**** ");
					dimensionesSuperficieTabla  = getValor("Largo") *  getValor("Ancho") ;
					dimensionesVolumenTabla = (dimensionesSuperficieTabla *  getValor("Fondo"));
					totalDimensionSuperficie = dimensionSuperfieciePatas + dimensionesSuperficieTabla;
					totalDimensionVolumen = dimensionVolumenPatas + dimensionesVolumenTabla;
					imprimeResultdo();
					break;
				case "0":
					System.exit(0);
					break;
				default:
					System.out.println("Opcion Errada!!!");
			}
		} while(!opAccion.equals("0"));
		
	}
	
	public void escribeMenu(){
		System.out.println("****Especificadores de Muebles********");
		System.out.println("* Escoge una opcion                  *");
		System.out.println("* 1. Generar medidas mesa de madera. *");
		System.out.println("* 0. Salir.                          *");
		System.out.println("**************************************");
	}
	
	public void imprimeError(String dimension, String valor) {
		System.out.println("*****Valor "+dimension+" no valida:  ---> "+ valor);
		System.out.println("Intentelo de nuevo");
	}

	public int getValor(String nombreEtiqueta) throws Exception {
		int result =0;
		String valorDimension = "";
		try {
			System.out.print(nombreEtiqueta+": ");
			valorDimension = sc.nextLine();
			result = Integer.parseInt(valorDimension) ;
		} catch (Exception e) {
			imprimeError(nombreEtiqueta, valorDimension);
			result = getValor(nombreEtiqueta);
		}
		return result;
	}
	
	public void imprimeResultdo() {
		System.out.println("Dimension totales");
		System.out.println("Superficie: "+ totalDimensionSuperficie +" Centimetros Cuadrados");
		System.out.println("Volumen: "+ totalDimensionVolumen+" Centimetros Cubicos");
		System.out.println("\n\n");
	}
	
    public static void main(String[] args) {
		
    	EspecificacionesMuebles muebles = new EspecificacionesMuebles();
		try {
			muebles.generaMedidasMuebles();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
